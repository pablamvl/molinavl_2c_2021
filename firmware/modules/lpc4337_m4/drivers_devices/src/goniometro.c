/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal header for goniometro on EDU-CIAA NXP
 **
 ** este driver es para la adquisición de la medida de ángulo.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *   MVLAP		Molina van Leeuwen Ana Pabla
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v0.0.1 initials initial version
 */



/*==================[inclusions]=============================================*/
#include "goniometro.h"


/*==================[macros and definitions]=================================*/
gpio_t sensor_pin;
#define TENSION_DER 2.1// tension a 75grados
#define TENSION_IZQ 1.2//tension a 0 grados
#define CANAL CH1
/*==================[internal data declaration]==============================*/
//conversion =FALSE;
//bool lectura = FALSE; //Bandera para la lectura de datos
float voltaje;
float angulo;
/*==================[internal functions declaration]=========================*/
void GoniometroStartanaloDigital(void);
void GoniometroLeer(float  *voltaje);//funcion de interrupcion valor leido canal a analogico
float ConvVoltajeAGrados();
float GoniometroLeeConvierte(float  *voltaje);

/*==================[internal data definition]===============================*/


/*==================[external data definition]===============================*/


analog_input_config mi_pote= {CANAL,AINPUTS_SINGLE_READ,&GoniometroLeer};// Typedef configuración puerto analógico/digital canal x

/*==================[internal functions definition]==========================*/

/*Función que inicializa la conversión de analógico a digital*/
void GoniometroStartanaloDigital(void){
	AnalogInputInit(&mi_pote);
	AnalogStartConvertion();
//	conversion =TRUE;
//	canal = CH1;
//	return TRUE;
}

//Lee el valor analogico
void GoniometroLeer(float  *voltaje){
	AnalogInputRead(CANAL,voltaje);
}

/*OTRA FORMA:conversion esperar lee valor
void GoniometroStartanaloDigital(void){
	AnalogInputInit(&mi_pote);
	AnalogInputReadPolling(CANAL,&voltaje);
}
/*
 *voltaje=AD_entero*3.3/1023
 *
 *
 * */



//convierte valor de tension en grados
float  ConvVoltajeAGrados(){
//convierte el valor en grados :(2.1-1.2)V--->75°, (voltaje-1.2)V----> x[°]
/*
	if(voltaje<TENSION_IZQ){
		angulo=0;
	}

	if(voltaje>TENSION_DER){
		angulo=75;
	}

	if((voltaje>TENSION_IZQ)&&(voltaje<TENSION_DER)){
		angulo=((voltaje-TENSION_IZQ)*75)/(TENSION_DER-TENSION_IZQ);
	}
*/
	if((voltaje>=TENSION_IZQ)&&(voltaje<=TENSION_DER)){
		angulo=((voltaje-TENSION_IZQ)*75)/(TENSION_DER-TENSION_IZQ);
	}


	return angulo;
}

/*Función que lee el valor analógico del canal y lo convierte a grados */
float  GoniometroLeeConvierte(float  *voltaje){
	GoniometroLeer(voltaje);
	angulo=ConvVoltajeAGrados();
	return angulo ;
}

/*==================[external functions definition]==========================*/

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
