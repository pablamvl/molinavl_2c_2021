/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal header for balanza on EDU-CIAA NXP
 **
 ** este driver es pesar las cajas.
 **
 **/
/*
 * Initials     Name
 * ---------------------------
 *   MVLAP		Molina van Leeuwen Ana Pabla
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v0.0.1 initials initial version
 */



/*==================[inclusions]=============================================*/
#include "balanza.h"
#include "bool.h"
/*==================[macros and definitions]=================================*/
#define TENSION_REF 3.3// tension de referencia en V ,máximo valor de voltaje
#define MAX_PESO 150 //valor maximo en kg
#define MAX_BITS 1024 // cantidad de bits del conversor AD
#define CANAL CH1 //canal por donde ingresan los datos analogicos de la balanza
/*==================[internal data declaration]==============================*/
bool conversion;// bandera para la conversion
float voltaje;
uint16_t valor;
float peso_Kg;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
void BalanzaLeer();//Función que lee el valor analógico del canal
float BalanzaConvVoltaje(uint16_t valor_D);//convierte valor digital a voltaje
/*==================[external data definition]===============================*/

analog_input_config mi_balanza= {CANAL,AINPUTS_SINGLE_READ,&BalanzaLeer};//configuración puerto analógico/digital

/*==================[internal functions definition]==========================*/

//Lee el valor analogico
void BalanzaLeer(void){
	conversion =false;
	AnalogInputRead(CANAL,&valor);
}

//conversion a voltaje:
float BalanzaConvVoltaje(uint16_t valor_D){

	float volt;
	volt=(float)(valor_D*TENSION_REF)/MAX_BITS; //dato convertido a voltaje
	return volt;
}

//Función que inicializa la conversión de analógico a digital
void BalanzaInitAnaloDigital(void){

	AnalogInputInit(&mi_balanza);
	conversion =true;
	AnalogStartConvertion();
	while(conversion){
	}
}

//convierte valor de voltaje a Kg
float ConvVoltajeAKG(float valor_voltaje){

	valor_voltaje=(float)BalanzaConvVoltaje(valor);//guardo valor digital en voltaje
	//convierte en Kg :3.3V--->150Kg, 0V----> 0Kg
	peso_Kg=valor_voltaje*MAX_PESO/TENSION_REF; // valor convertido en kg

	return peso_Kg;
}


float BalanzaLeeConvierte(uint16_t *valor){
	float volt;
	float peso;
	volt=BalanzaConvVoltaje(valor);//valor es un entero, voltaje un flotante
	peso=ConvVoltajeAKG(volt);
	return peso;
}

/*==================[external functions definition]==========================*/

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
