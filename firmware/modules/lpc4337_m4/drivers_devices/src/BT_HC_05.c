/* Copyright 2016, 
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastián Mateos
 * smateos@ingenieria.uner.edu.ar	
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup BT_HC_05 BT_HC_05
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * MVLAP		Molina van Leeuwen Ana Pabla
 */

/*==================[inclusions]=============================================*/
#include "BT_HC_05.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
	serial_config p;
/*==================[internal functions declaration]=========================*/
/*void BTHC_05Init(serial_config *puerto);
void BTHC_05SendByte(uint8_t puerto,uint8_t* dato);
uint8_t BTHC_05ReadByte(uint8_t puerto,uint8_t* dato);
void BTHC_05SendByte(uint8_t puerto,uint8_t* dato);*/
/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void BTHC_05Init(serial_config *puerto){
/*
	p.port = puerto->port;
	p.baud_rate = puerto->baud_rate;
	p.pSerial = puerto->pSerial;*/
	UartInit(puerto);
}


uint8_t  BTHC_05ReadByte(uint8_t puerto,uint8_t* dato){
	UartReadByte(puerto,dato);
}


void BTHC_05SendString(uint8_t puerto,uint8_t* dato){

	UartSendString(puerto,dato);

}

void BTHC_05SendByte(uint8_t puerto,uint8_t* dato){

	UartSendByte(puerto,dato);
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
