/* Copyright 2021,
 * Levrino Micaela
 * micaela_levrino@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef HC_SR4_H
#define HC_SR4_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Hc_sr4
 ** @{ */

/** \brief driver capable of measuring the proximity of an object by ultrasound.
 **
 ** This is a driver for a HC_SR4 sensor connected to the board
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  ML	        Micaela Levrino
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200415 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/

#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Initialization function to control the ultrasound distance meter belt on the EDU-CIAA BOARD
 * This function initialize the peripheral echo as an input periphery and trigger as an output periphery.
 * @param[in] echo
 * @param[in] trigger
 * @return 1 (true) if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);
/** @fn int16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Function that reads distance in cm
 * This function implements the function to determine the distance in cm.
 * @param[in] No parameter
 * @return Distance in cm
 */
int16_t HcSr04ReadDistanceCentimeters(void);
/** @fn int16_t HcSr04ReadDistanceInches(void)
 * @brief Function that reads distance in inches
 * This function implements the function to determine the distance in inches.
 * @param[in] No parameter
 * @return Distance in inches
 */
int16_t HcSr04ReadDistanceInches(void);
/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief Function to de-initialize
 * This function de-initialize the peripheral echo and trigger.
 * @param[in] echo
 * @param[in] trigger
 * @return 1 (true) if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef HC_SR4_H */
