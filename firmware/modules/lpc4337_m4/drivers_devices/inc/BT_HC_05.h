/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 * Sebastián Mateos
 * sebastianantoniomateos@gmail.com
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef BT_HC_05_H
#define BT_HC_05_H

/** \brief UART Bare Metal driver for the peripheral in the EDU-CIAA Board.
 **
 ** Driver que controla comunicacion puerto serie del dispositivo Bluetooth
 **
 **/

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup BT_HC_05 BT_HC_05
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * MVLAP		Molina van Leeuwen Ana Pabla
 */


/*==================[inclusions]=============================================*/
//#include "stdint.h"
#include "chip.h"
#include "uart.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn uint32_t BTHC_05Init(serial_config *puerto)
 * @brief Inicializar BT_HC_05
 * @param[in] port Puerto para inicializar (RS232)
 */
void  BTHC_05Init(serial_config *puerto);


/** @fn uint32_t UartReadStatus(uint8_t puerto)
 * @brief Leer el estado del puerto
 * @param[in] port Puerto que se desea leer
 */
uint8_t BTHC_05ReadByte(uint8_t puerto, uint8_t* dato);


/** @fn BTHC_05SendByte(uint8_t puerto,uint8_t* dato)
 * @brief envía datos BT_HC_05
 * @param[in] port Puerto para enviar
 */

void BTHC_05SendString(uint8_t puerto,uint8_t* dato);


/** @fn BTHC_05SendByte(uint8_t puerto,uint8_t* dato)
 * @brief envía datos BT_HC_05
 * @param[in] port Puerto para enviar
 */
void BTHC_05SendByte(uint8_t puerto,uint8_t* dato);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef BT_HC_05 */

