/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef GONIOMETRO_H
#define GONIOMETRO_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup GONIOMETRO goniometro
 ** @{ */

/** @brief Bare Metal header for goniometro on EDU-CIAA NXP
 **
 ** este driver es para la adquisición de la medida de ángulo.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *   MVLAP		Molina van Leeuwen Ana Pabla
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "gpio.h"
#include "analog_io.h"
#include "bool.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn void GoniometroStartanaloDigital(void)
 * @brief *Función que inicializa la conversión de analógico a digital
 * @return null
 */
void GoniometroStartanaloDigital(void);


/** @fn void GoniometroLeer(uint16_t *voltaje)
 * @brief Funticion que lee el valor analogico
 * @param[in] puntero al valor leid
 * @return null
 */
void GoniometroLeer(float  *voltaje);


/** @fn uint16_t ConvertOhmAGrados(uint16_t voltaje)
 * @brief Función que lee el pin y devuelve el valor convertido de analógico a digital en unidades de gravedad.
 * @param[in] valor analogico
 * @return valor convertido
 */
float  ConvVoltajeAGrados();


/** @fn uint16_t GoniometroLeeConvierte(uint16_t *voltaje)
 * @brief Función que lee el pin y devuelve el valor convertido de analógico a digital en grados
 * @param[in] No hay parámetros
 * @return valor convertido
 */
float  GoniometroLeeConvierte(float  *voltaje);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef GONIOMETRO_H */

