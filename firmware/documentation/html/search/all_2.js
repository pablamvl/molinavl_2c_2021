var searchData=
[
  ['balanza',['balanza',['../group___b_a_l_a_n_z_a.html',1,'']]],
  ['balanzaleeconvierte',['BalanzaLeeConvierte',['../group___b_a_l_a_n_z_a.html#ga7eec191050aa743933cb736037ff5141',1,'BalanzaLeeConvierte(uint16_t *valor):&#160;balanza.c'],['../group___b_a_l_a_n_z_a.html#ga7eec191050aa743933cb736037ff5141',1,'BalanzaLeeConvierte(uint16_t *valor):&#160;balanza.c']]],
  ['balanzastartanalodigital',['BalanzaStartanaloDigital',['../group___b_a_l_a_n_z_a.html#gafa5bd577a81fcb82cc9fdc152b1f585b',1,'BalanzaStartanaloDigital(void):&#160;balanza.c'],['../group___b_a_l_a_n_z_a.html#gafa5bd577a81fcb82cc9fdc152b1f585b',1,'BalanzaStartanaloDigital(void):&#160;balanza.c']]],
  ['bare_20metal_20example_20header_20file',['Bare Metal example header file',['../group___baremetal.html',1,'']]],
  ['baud_5frate',['baud_rate',['../structserial__config.html#ae28e9ec22e61f2fd869555513c0857c0',1,'serial_config']]],
  ['bool',['bool',['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html',1,'(Global Namespace)']]],
  ['bt_5fhc_5f05',['BT_HC_05',['../group___b_t___h_c__05.html',1,'']]]
];
