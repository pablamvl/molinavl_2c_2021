var searchData=
[
  ['balanza',['balanza',['../group___b_a_l_a_n_z_a.html',1,'']]],
  ['bare_20metal_20example_20header_20file',['Bare Metal example header file',['../group___baremetal.html',1,'']]],
  ['baud_5frate',['baud_rate',['../structserial__config.html#ae28e9ec22e61f2fd869555513c0857c0',1,'serial_config']]],
  ['bool',['bool',['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html#gabb452686968e48b67397da5f97445f5b',1,'bool():&#160;bool.h'],['../group___bool.html',1,'(Global Namespace)']]],
  ['bt_5fhc_5f05',['BT_HC_05',['../group___b_t___h_c__05.html',1,'']]],
  ['bthc_5f05init',['BTHC_05Init',['../group___b_t___h_c__05.html#gaccac2679ea8d364ea4b32ce2a03d74db',1,'BT_HC_05.c']]],
  ['bthc_5f05sendbyte',['BTHC_05SendByte',['../group___b_t___h_c__05.html#ga80be56ea1f752ffce86e00cdc4554533',1,'BT_HC_05.c']]]
];
