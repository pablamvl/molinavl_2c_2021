/*! @mainpage ULTRASOUND METER 1.0
 * This appliaction shows the distance (in centimeters) between an objet and the sensor, in a display. This uses two devices, an ultrasound meter and a display
 *
 *
 * \section genDesc General Description
 *
 * This application makes the ultrasound meter
 *
 * \section hardConn Hardware Connection
 *
 * |    HC-SR4     	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|

 *
 * |   ITS_E0803   	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	    	| 	LCD1		|
 * | 	D2	    	| 	LCD2		|
 * | 	D3 	  	    | 	LCD3		|
 * | 	D4  	 	| 	LCD4 		|
 * | 	SEL_0 	 	| 	GPIO1 		|
 * | 	SEL_1 	 	| 	GPIO3 		|
 * | 	SEL_2 	 	| 	GPIO5 		|
 * | 	+5v 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/09/2021  | Document creation		                         |
 * | 3/09/2021  | Ultrasound device configurated  and tested     |
 * | 10/09/2021 | New display configuration added and tested     |
 * | 16/09/2021 | Document completed	                         |
 *
 * @author Maximiliano Javier Cantarutti
 *
 */

/*==================[inclusions]=============================================*/
#include "ultrasound_meter.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
int16_t HcSr04ReadDistanceCentimeters();
/* La funcion HcSr04ReadDistanceCentimeters, cuando es llamada, sensa el dispositivo ultrasonido y acumula e microsegundos el tiempo
 * en que el mismo devuelve una respuesta en alto. No recibe parámetro alguno y devuelve un entero, que es la distancia en cm *
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);
/*La funcion HcSr04Init se encarga de asignar los pines para la lectura del módulo ultrasonido.
 * Siendo echo el impulso recibido por la CIAA que significa la detección de un elemento, y trigger el disparo.
 * Recibe como parametro dos variables tipo gpio_t y devuelve 1 y 0.*/

/*==================[internal functions declaration]=========================*/



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{   uint8_t teclas;
    int16_t distancia;
    uint8_t activo =0;
    uint8_t hold=1;
	SystemClockInit();

	/*configuración LCD*/
	gpio_t pines_config[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	gpio_t *ptr_pines_conf = pines_config;

	/*Inicializacion del display, leds, switches y sensor*/
	ITSE0803Init(ptr_pines_conf);
	LedsInit();
	SwitchesInit();
	/*configuración ultrasonido*/
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);


	while(1)
    {

     LedOff(LED_RGB_B);
     LedOff(LED_2);
     LedOff(LED_1);
     LedOff(LED_3);

     teclas  = SwitchesRead();

     switch(teclas) {
            case SWITCH_1:
	              activo=!activo;
	              LedOn(LED_RGB_R); /*Indican que la tecla fue presionada*/
	              DelayMs(100);
	              LedOff(LED_RGB_R);

            break;

            case SWITCH_2:
           	      hold=!hold;
           	      LedOn(LED_RGB_B); /*Indican que la tecla fue presionada*/
           	   	  DelayMs(100);
           	   	  LedOff(LED_RGB_B);
            break;
     }

     if (activo){

    	 distancia = HcSr04ReadDistanceCentimeters(); /*Si se solicita, llama a la funcion para leer disancia*/

    	if(hold)
    	{
    		if(distancia<10) {
      	  LedOn(LED_RGB_B);
           }
       else  {
       if(distancia<20)
      	  {LedOn(LED_RGB_B);
         	  LedOn(LED_1); }
      	  else{
              if(distancia<30) {
            LedOn(LED_2);
  		  LedOn(LED_RGB_B);
  		  LedOn(LED_1);   }
            else {
          	  LedOn(LED_2);
          	  LedOn(LED_RGB_B);
          	  LedOn(LED_1);
          	  LedOn(LED_3);
            }}}
    	ITSE0803DisplayValue(distancia); /*devuelve el valor de la distancia en cm*/
    	}
     }
     else {
    	 ITSE0803DisplayValue(0);
          LedOff(LED_RGB_B);
          LedOff(LED_2);
          LedOff(LED_1);
          LedOff(LED_3);
     }
     DelaySec(1);
     distancia=0;


}
    return 0;
}

/*==================[end of file]============================================*/

