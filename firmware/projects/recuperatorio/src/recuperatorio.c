/*! @mainpage Examen recuperatorio Molina van Leeuwen
 *
 * \section genDesc General Description
 *
 * Proyecto: Examen recuperatorio
 * Diseño de un dispositivo que se utiliza como control de un sistema
 * de llenado de cajas en una línea de producción.
 * sistema cuenta con dos cintas transportadoras, una balanza y sensor infrarrojo TCRT5000.
 * Funcionamiento:
 *
 * - Cinta transportadora 1 debe llevar las cajas vacías hasta la posición de llenado,
 *   la cual es indicada por el sensor infrarrojo: prende LED 1.
 *
 * - Una vez que la caja vacía llegue a la posición de llenado, se detiene la cinta transportadora 1
 *   y encender la cinta transportadora 2 (simulada con el LED 2) la cual transporta las piezas que llenarán la caja.
 *
 *  -La balanza, ubicada en la posición de llenado, permite medir el peso de la caja.se llenan las cajas
 *   hasta alcanzar un peso de 20kg .
 *
 *  -Al alcanzar dicho peso se detiene la cinta 2 y se enciende la cinta 1,hasta que la próxima
 *   caja vacía llegue a la posición de llenado.
 *
 *  -Luego de llenar cada caja se envia además a través del puerto serie : el tiempo de llenado de dicha caja,
 *   con el formato:    "Tiempo de llenado de caja 1 XX seg".
 *
 *  -una vez llenado un lote completo de cajas (15 cajas), se debe informar por el puerto serie los tiempos
 *   de llenado máximo y mínimo de dicho lote, con el formato:    "Tiempo de llenado máximo: XX seg"
 *                                                                "Tiempo de llenado mínimo: XX seg"
 *  -se utiliza las teclas 1 y 2 para iniciar y detener el sistema.
 *
 * Drivers de dispositivos utilizados:
 *                      -TCRT5000
 *			            -balanza
 *			            -led
 *			            -switch
 *
 * \section hardConn Hardware Connection
 *
 * |   tcrt5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 *
 * |    balanza     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |	VCC	     	| +3.3V (VDDA)	|
 * | 	GND 	 	| 	GNDA  		|
 * | 	Canal	 	| 	  CH1	    |
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                            |
 * |:----------:|:-------------------------------------------------------|
 * | 11/11/2021	 | Creación del documento		                         |
 * |11/11/2021	 |                 		                                 |

 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/recuperatorio.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "bool.h"
#include "tcrt5000.h"
#include "balanza.h"
#include "led.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUD_RATE 115200
#define CANT_CAJAS 15 // UN LOTE DE CAJAS
#define PERIODO 1000 // en ms

/*==================[internal data definition]===============================*/
uint16_t valor;//valor analogico leido
uint16_t peso_actual;// valor analogico convertido a kg
//uint16_t tiempo_max;//tiempo maximo
//uint16_t tiempo_min;//tiempo minimo
//uint32_t tiempo_actual;// tiempo actual
uint16_t peso;// valor en Kg
uint8_t contador_timer=0;//guarda las veces que corre el timer
bool estado_pin=false;// para detectar una caja
bool estado_anterior=false;// para detectar una caja

uint8_t contador_cajas;
bool activo_sistema=FALSE;//bandera de encendido/apagado del sistema
bool posicion_llenado=FALSE;// bandera de posicion de llenado, sensor detecta caja
//bool lectura = FALSE; //Bandera para la lectura de datos
uint16_t cajas[]={};
uint16_t tiempo_llenado[]={};
/*==================[internal functions declaration]=========================*/
//DECLARAMOS FUNCIONES
void SisInit(void);
void DatosAPC();//envia por la uart
void functionSwitch1();// enciende el sistema
void functionSwitch2();// apaga el sistema
void TiempoLlenado();
/*==================[external data definition]===============================*/
gpio_t pin_sensor=GPIO_T_COL0;// pin sensor tcrt5000

timer_config my_timer = {TIMER_A,PERIODO,&TiempoLlenado};
serial_config mi_uart = {SERIAL_PORT_PC,BAUD_RATE,&DatosAPC};

/*==================[external functions definition]==========================*/
//Función que controla la tecla 1 para el encendido del sistema.
void functionSwitch1(){
	activo_sistema= TRUE;
}

void functionSwitch2(){
	activo_sistema= FALSE;
}

//Funcion de interrupcion por la uart
 void DatosAPC(){
//tiempo de llenado

//falta  calcular ek tiempo de llenado
	uint8_t i;
	for(i=CANT_CAJAS;i<0;i++){
		UartSendString(SERIAL_PORT_PC,(uint8_t *) "Tiempo de llenado caja ");
		UartSendString (SERIAL_PORT_PC, UartItoa(cajas[i], 10));
		UartSendString (SERIAL_PORT_PC, UartItoa(tiempo_llenado[i], 10));//falta  calcular ek tiempo de llenado
		UartSendString (SERIAL_PORT_PC,(uint8_t *)"seg");
		UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
		}
}

//Funcion de interrupcion por el timer
void TiempoLlenado(){
	contador_timer++;
	//llena las cajas
	estado_pin=Tcrt5000State();//lee el estado del pin
	LedOn(LED_1); //Led 1 cinta 1 lleva a la posicion de llenado
	if (estado_actual == 0 && estado_anterior == 1) {
			posicion_llenado= TRUE;
			contador_cajas++;
			peso_actual=BalanzaLeeConvierte(valor);// valor analogico a kg
	}
		estado_anterior=estado_actual;
}


void SisInit(void){
/***************configuracion pines e inicializacion:*************/
		SystemClockInit();
	//Sensor:
		Tcrt5000Init(pin_sensor);// inicializo el pin como entrada
	//Teclas:
		SwitchesInit();
	//Leds:
		LedsInit();
	// balanza:
		BalanzaStartanaloDigital();
	//Timer
		TimerInit(&my_timer);
		TimerStart(TIMER_A);
	//uart
		UartInit(&mi_uart);

		peso=0;// peso de la caja, 0= caja vacia

}

int main(void){

/***************configuracion pines e inicializacion:*************/
	SisInit();
	//Control de teclas por interrupciones:
	SwitchActivInt(SWITCH_1 ,&functionSwitch1);
	SwitchActivInt(SWITCH_2 ,&functionSwitch2);

    while(1){
  //
    		if (activo_sistema){

    		// Led 1 cinta 1 lleva a la posicion de llenado


    			//cuando llega, se detiene cinta 1, enciende cinta 2---- led 2
    		}
// no llegue a calcular el maximo y minimo de los tiempo de llenado
/*
 * NO LLEGUE A TERMINAR EL EXAMEN NI A PLANTEAR BIEN LAS FUNCIONES
 *
 * */
 */

	return 0;
}

/*==================[end of file]============================================*/

