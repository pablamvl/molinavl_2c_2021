/*! @mainpage Examen recuperatorio Molina van Leeuwen
 *
 * \section genDesc General Description
 *
 * Proyecto: Examen recuperatorio
 * Diseño de un dispositivo que se utiliza como control de un sistema
 * de llenado de cajas en una línea de producción.
 * sistema cuenta con dos cintas transportadoras, una balanza y sensor infrarrojo TCRT5000.
 * Funcionamiento:
 *
 * - Cinta transportadora 1 debe llevar las cajas vacías hasta la posición de llenado,
 *   la cual es indicada por el sensor infrarrojo: prende LED 1.
 *
 * - Una vez que la caja vacía llegue a la posición de llenado, se detiene la cinta transportadora 1
 *   y encender la cinta transportadora 2 (simulada con el LED 2) la cual transporta las piezas que llenarán la caja.
 *
 *  -La balanza, ubicada en la posición de llenado, permite medir el peso de la caja.se llenan las cajas
 *   hasta alcanzar un peso de 20kg .
 *
 *  -Al alcanzar dicho peso se detiene la cinta 2 y se enciende la cinta 1,hasta que la próxima
 *   caja vacía llegue a la posición de llenado.
 *
 *  -Luego de llenar cada caja se envia además a través del puerto serie : el tiempo de llenado de dicha caja,
 *   con el formato:    "Tiempo de llenado de caja 1 XX seg".
 *
 *  -una vez llenado un lote completo de cajas (15 cajas), se debe informar por el puerto serie los tiempos
 *   de llenado máximo y mínimo de dicho lote, con el formato:    "Tiempo de llenado máximo: XX seg"
 *                                                                "Tiempo de llenado mínimo: XX seg"
 *  -se utiliza las teclas 1 y 2 para iniciar y detener el sistema.
 *
 * Drivers de dispositivos utilizados:
 *                      -TCRT5000
 *			            -balanza
 *			            -led
 *			            -switch
 * \section hardConn Hardware Connection
 *
 *
 * |   tcrt5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 *
 * |    balanza     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |	VCC	     	| +3.3V (VDDA)	|
 * | 	GND 	 	| 	GNDA  		|
 * | 	Canal	 	| 	  CH1	    |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                            |
 * |:----------:|:-------------------------------------------------------|
 * | 11/11/2021	 | Creación del documento		                         |
 * | 11/11/2021	 |                 		                                 |

 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

#ifndef _RECUPERATORIO_H
#define _RECUPERATORIO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _RECUPERATORIO_H*/

