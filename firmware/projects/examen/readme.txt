﻿Descripción del Proyecto: examen

Se utiliza el de ultrasonido HC-SR4 se utiliza para el sensado de los objetos,ubicado en distintas
orientaciones por medio de un motor. El sensor realiza un “barrido” de todos los objetos frente al móvil.
El HC-SR04 se encuentra sobre un potenciómetro solidario al motor que le permite conocer su orientación
 a partir de medir el voltaje en el punto medio de dicho potenciómetro.
 El sensor se mueve en un arco de 75°, realizando una medicion cada 15°.
Estas maediciones son transmitidas por puerto serie a la PC con el formato:
                      [grado]º objeto a [distancia] cm
El disposito realiza lo siguiente :
-Identifica el ángulo en el que se encuentra el objeto más cercano en cada
ciclo de barrido, y se enviar la cadena:
 						Obstáculo en Xº
-Se controla el motor, cambiando de direccion al llegar a cada extremo (75° y 0°)
 Leds para simular el control del mismo:
                      - led RGB rojo: indica que se está girando a la derecha.
                      - led RGB verde: indica que se está girando a la izquierda.

 Drivers de dispositivos utilizados:
			            -HC-SR4(ultrsonido)
			            -led
       		            -goniometro