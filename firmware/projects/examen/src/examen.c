/*! @mainpage Examen Molina van Leeuwen
 *
 * \section genDesc General Description
 *
 * Proyecto: Examen
 * Diseño de un dispositivo que se utilizará como detector de objetos cercanos (sonar)
 * en un robot móvil.
 * Se utiliza el de ultrasonido HC-SR4 se utiliza para el sensado de los objetos,ubicado en distintas
 * orientaciones por medio de un motor. El sensor realiza un “barrido” de todos los objetos frente al móvil.
 * El HC-SR04 se encuentra sobre un potenciómetro solidario al motor que le permite conocer su orientación
 * a partir de medir el voltaje en el punto medio de dicho potenciómetro.
 * El sensor se mueve en un arco de 75°, realizando una medicion cada 15°.
 * Estas maediciones son transmitidas por puerto serie a la PC con el formato:
 *                      [grado]º objeto a [distancia] cm
 * El disposito realiza lo siguiente :
 *-Identifica el ángulo en el que se encuentra el objeto más cercano en cada
 * ciclo de barrido, y se enviar la cadena:
 * 						Obstáculo en Xº
 *-Se controla el motor, cambiando de direccion al llegar a cada extremo (75° y 0°)
 * Leds para simular el control del mismo:
 *                      - led RGB rojo: indica que se está girando a la derecha.
 *                      - led RGB verde: indica que se está girando a la izquierda.
 *
 * Drivers de dispositivos utilizados:
 *			            -HC-SR4(ultrsonido)
 *			            -led
 *			            -goniometro
 *
 * \section hardConn Hardware Connection
 *
 * |    HC-SR4     	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	VCC	 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * |  goniometro    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH1	        |
 * |    GND         |   GNDA        |
 * |    VCC         | 3.3V (VDDA)   |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                            |
 * |:----------:|:-------------------------------------------------------|
 * | 01/11/2021	 | Creación del documento		                         |
 * | 01/11/2021	 |                 		                                 |

 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

/*==================[inclusions]=============================================*/
#include "examen.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "bool.h"
#include "hc_sr4.h"
#include "goniometro.h"
#include "led.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUD_RATE 115200
#define CANT_ANGULOS 6
#define ANGULO_MAXIMO 75 //Angulo correspondiente a 2.1V
#define ANGULO_MINIMO 0 //Angulo correspondiente a 1.2V
#define PERIODO 250 // en ms

/*==================[internal data definition]===============================*/
uint32_t vector_grado[]={0,15,30,45,60,75};
uint16_t voltaje;
uint8_t angulo_actual; //Valor del ángulo medido
uint16_t minima_distancia_aux=0;//distancia minima actual
uint32_t angulo_obj_cercano;// angulo en el que se encuentre el objeto más cercano
uint16_t angulo_minimo_actual=0;//Valor del ángulo minimo medido actual
//uint16_t contador_distancia;// distancia en cm

bool tiempo_FLAG=FALSE;//BANDERA DE CONVERSION
bool lectura = FALSE; //Bandera para la lectura de datos

uint16_t contador_distancia[]={};

/*==================[internal functions declaration]=========================*/
//DECLARAMOS FUNCIONES
void SisInit(void);
//void StarConv(void);
//void Leerdatos();
void DatosAPC();//funcion interrupcion uart enviar datos puerto
void ObjetoCercano();//funcion interrupcion cada 250ms

/*==================[external data definition]===============================*/
gpio_t echo={GPIO_T_FIL2};
gpio_t trigger={GPIO_T_FIL0};
timer_config my_timer = {TIMER_A,PERIODO,&ObjetoCercano};
serial_config mi_uart = {SERIAL_PORT_PC,BAUD_RATE,&DatosAPC};
//tiempo=N/fclk
/*==================[external functions definition]==========================*/
//Funcion de interrupcion por la uart
void DatosAPC(){
//mediciones transmitidas por puerto serie a la PC:
//gradoº objeto a distancia cm
 //   lectura =TRUE;
	uint8_t i;
	for(i=CANT_ANGULOS;i<0;i++){
		UartSendString (SERIAL_PORT_PC, UartItoa(vector_grado[i], 10));
		UartSendString (SERIAL_PORT_PC,(uint8_t *)"º");
		UartSendString (SERIAL_PORT_PC,(uint8_t *)"objeto a");
		UartSendString (SERIAL_PORT_PC, UartItoa(contador_distancia[i], 10));
		UartSendString (SERIAL_PORT_PC,(uint8_t *)"cm");
		UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");
		}
}

// cada 15 grados hacer la medicion
//Funcion de interrupcion por el timer
void ObjetoCercano(){
	angulo_actual= GoniometroLeeConvierte(voltaje);//lee el valor analogico y luego lo convierte a grados
	tiempo_FLAG=TRUE;
	uint8_t j;
//	uint8_rango=3;//+- defino un rango para comparar
	for(j=0;j<5;j++){
		if((angulo_actual>(vector_grado[j]-rango)) && (angulo_actual<(vector_grado[j]+rango))){
		//	if(((angulo_actual+rango)>(vector_grado[j])) && ((angulo_actual-rango)<(vector_grado[j]))){// poner rango
				contador_distancia[j]=HcSr04ReadDistanceCentimeters();
		}

		if(contador_distancia[j]<minima_distancia_aux){
			minima_distancia_aux=contador_distancia[j];
			angulo_obj_cercano=vector_grado[j];
		}
	}
}



void SisInit(void){
/***************configuracion pines e inicializacion:*************/
		SystemClockInit();
	//Sensor:
		HcSr04Init(echo,trigger);
	//Leds:
		LedsInit();
	// goniometro:
		GoniometroStartanaloDigital();
	//Timer
		TimerInit(&my_timer);
		TimerStart(TIMER_A);
	//uart
		UartInit(&mi_uart);
}

int main(void){

/***************configuracion pines e inicializacion:*************/
	SisInit();
    while(1){
  // INFORMAR MEDICIONES
    		if (tiempo_FLAG){
    	//angulo_actual= GoniometroLeeConvierte(&voltaje);
		//angulo en el que se encuentre el objeto más cercano en cada ciclo de barrido:
		//Obstáculo en 15º
			UartSendString(SERIAL_PORT_PC,(uint8_t *) "Obstáculo en");
			UartSendString(SERIAL_PORT_PC, UartItoa(angulo_obj_cercano,10));
			UartSendString (SERIAL_PORT_PC,(uint8_t *)"º");
			UartSendString(SERIAL_PORT_PC, (uint8_t *)"\r\n");

			if(angulo_actual==ANGULO_MAXIMO){
				LedOn(LED_RGB_R); /*Led rojo motor girando hacia la derecha*/
			}
			if(angulo_actual==ANGULO_MINIMO){
				LedOn(LED_RGB_G);/*Led verde motor girando hacia la izquierda*/
			}
//		lectura = FALSE;
			}

		LedOff(LED_RGB_G);
		LedOff(LED_RGB_R);
	return 0;
    }
}

/*==================[end of file]============================================*/

