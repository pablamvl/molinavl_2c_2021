/*! @mainpage Examen Molina van Leeuwen
 *
 * \section genDesc General Description
 *
 * Proyecto: Examen
 * Diseño de un dispositivo que se utilizará como detector de objetos cercanos (sonar)
 * en un robot móvil.
 * Se utiliza el de ultrasonido HC-SR4 se utiliza para el sensado de los objetos,ubicado en distintas
 * orientaciones por medio de un motor.
 * El sensor realiza un “barrido” de todos los objetos frente al móvil.
 * El HC-SR04 se encuentra sobre un potenciómetro solidario al motor que le permite conocer su orientación
 * a partir de medir el voltaje en el punto medio de dicho potenciómetro.
 * El sensor se mueve en un arco de 75°, realizando una medicion acda 15°.
 * Estas maediciones son transmitidas por puerto serie a la PC con el formato:
 *                      [grado]º objeto a [distancia] cm
 * El disposito realiza lo siguiente :
 *-Identifica el ángulo en el que se encuentra el objeto más cercano en cada
 * ciclo de barrido, y se enviar la cadena:
 * 						Obstáculo en Xº
 *-Se controla el motor, cambiando de direccion al llegar a cada extremo (75° y 0°)
 * Leds para simular el control del mismo:
 *                      - led rojo: indica que se está girando a la derecha.
 *                      - led verde: indica que se está girando a la izquierda.
 *
 * Drivers de dispositivos utilizados:
 *			            -HC-SR4(ultrsonido)
 *			            -led
 *			            -goniometro
 * \section hardConn Hardware Connection
 *
 * |    HC-SR4     	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	VCC	 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * |  goniometro    |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH1	        |
 * |    GND         |   GNDA        |
 * |    VCC         | 3.3V (VDDA)   |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                            |
 * |:----------:|:-------------------------------------------------------|
 * | 01/11/2021	 | Creación del documento		                         |
 * | 01/11/2021	 |                 		                                 |

 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

#ifndef _EXAMEN_H
#define _EXAMEN_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EXAMEN_H*/

