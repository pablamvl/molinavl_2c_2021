﻿Descripción del Proyecto:Proyecto Abierto

 A partir de la detección de un cambio en la aceleración del paciente por medio de un
 sensor de aceleración triaxil(acelerómetro), de determina el inicio y final de la pasada
 de un paciente en un laboratorio de marcha.
 Señal analógica a utilizar: señal del acelerómetro MMA7260Q, colocada en el paciente.
 La salida de datos y el control de la aplicación, se realiza por una PC,
 con vía comunicación con el puerto serie (Bluetooth).
 La aplicación realizar lo siguiente:
 - Tecla “a” activa el acelerómetro, una vez el paciente éste parado para comenzar el análisis de la marcha.
 - Análisis de señal analógica (aceleración).
 	○ Se determinará el inicio de la marcha por medio de la señal analógica.
 	○ Fin de la marcha: se desactiva el acelerómetro cuando llegue a los 7m de distancia recorrida.
    ○ Otros: se contará los pasos y se mandará al puerto.