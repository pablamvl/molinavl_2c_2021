/*! @mainpage Proyecto_Abierto_EDU_1
 *
 * \section genDesc General Description
 * Proyecto Abierto:
 * Formado por dos proyectos: Proyecto_Abierto_EDU_1 y Proyecto_Abierto_EDU_2
 * Descripcion del proyecto en general:
 * A partir de la detección de un cambio en la aceleración del paciente por medio de un
 * sensor de aceleración triaxil(acelerómetro), de determina el inicio y final de la pasada
 * de un paciente en un laboratorio de marcha.
 *
 * Señal analógica a utilizar: señal del acelerómetro MMA7260Q, colocada en el paciente.
 * La salida de datos y el control de la aplicación, se realiza por una PC,
 * con vía comunicación con el puerto serie (Bluetooth).
 * La aplicación realizar lo siguiente:
 * - Tecla “a” activa el acelerómetro, una vez el paciente éste parado para comenzar el análisis de la marcha.
 * - Análisis de señal analógica (aceleración).
 *	○	Se determinará el inicio de la marcha por medio de la señal analógica.
 *	○	Fin de la marcha: se desactiva el acelerómetro cuando llegue a los 7m de distancia recorrida.
 *	○	Otros: se contará los pasos y se mandará al puerto.
 *
 * Función especifica del Proyecto_Abierto_EDU_1:
 * "Pasa mano" entre el Bluetooth Master y la PC
 *
 * \section hardConn Hardware Connection
 *
 * |  HC-05 M   	| EDU-CIAA N° 1	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 * |  HC-05 S   	| EDU-CIAA N° 2	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 * |  MMA7260Q   	| EDU-CIAA N° 2	|
 * |:--------------:|:--------------|
 * | 	PINX	 	| 	  CH1	    |
 * | 	PINY	 	| 	  CH2       |
 * | 	PINZ	 	| 	  CH3       |
 * | 	GS1	 	    | 	T_COL0	  	|
 * | 	GS2	       	| 	T_COL1	  	|
 * |	VCC	     	| +3.3V (VDDA)	|
 * |	GND	 	    |    GNDA       |
 * |	SM	 	    |    +3.3V      |
 *
 *
 *
 * @section changelog Changelog Proyecto_Abierto_EDU_1
 *
 * |   Date	     | Description                                    |
 * |:-----------:|:-----------------------------------------------|
 * | 11/10/2020	 | Document creation		                      |
 * | 21/10/2021  | Inializacion  drivers                          |
 * | 28/10/2021  | en proceso                                     |
 * | 03/11/2021  | compila                                        |
 * | 04/11/2021  | revisar comunicacion BT master-slave           |
 * | 04/11/2021  | compila,no hay trasmision de datos por BT      |
 * | 10/11/2021  | compila,se arregla trasmision de datos por BT  |
 * | 10/11/2021  |  se prueba comunicacion con BT celular         |
 * | 12/11/2021  |  comunicacion entre bluetooths M-S funciona    |
 * | 12/11/2021  |  se observa la grafica de acelerarcion         |
 * | 12/11/2021  |  revisar calculo distancia                     |
 * | 12/11/2021  |  calculo distancia  funcionando                |
 * | 14/11/2021  |  proyecto  funcionando                         |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */
/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Abierto_EDU_1.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "bool.h"
#include "uart.h"
#include "BT_HC_05.h"
#include "led.h"
/*==================[macros and definitions]=================================*/

#define BAUD_RATE 115200 //Velocidad de transmisión
#define TIEMPO_MUESTREO 250 //tiempo de muestreo
/*==================[internal data definition]===============================*/
uint8_t tecla_inicio;// para guardar el dato que la PC le envíe
uint8_t datos_recibidos_BTM;//datos recibidos desde BT master
bool grafica_flg = FALSE;//para activar el graficador
/*==================[internal functions declaration]=========================*/

void UartPCaBTM(void);//lee el dato recibido por PC envia a BT master
void UartBTMaPC(void);////lee el dato recibido de BTM master
void ActivaGraficacion();//funcion que activa la graficacion
/*==================[external data definition]===============================*/
serial_config Uart_PC = {SERIAL_PORT_PC,BAUD_RATE,&UartPCaBTM};
serial_config Uart_Bluetooth_M = {SERIAL_PORT_P2_CONNECTOR,BAUD_RATE,&UartBTMaPC};

/*==================[external functions definition]==========================*/
/****Funcion que activa la graficacion****/
void ActivaGraficacion(){
	grafica_flg = TRUE;
}/*FIN GRAFICAR---*/

/****Funciones interrupcion cuando recibe dato por PC y envía a BT master****/
//funcion interrupcion de llegada de dato a la PC
void UartPCaBTM(void){
// lee el dato de la PC y envia por BT
	UartReadByte(SERIAL_PORT_PC,&tecla_inicio);//lee dato por la uart
	LedOn(LED_1);
	BTHC_05SendByte(SERIAL_PORT_P2_CONNECTOR,&tecla_inicio);//envia por BT
	LedOn(LED_2);
}/*FIN PC--> BT master---*/

/****Funcion interrupcion cuando recibe dato por BT master y envía a PC****/
//recibe dato de BTM master
void UartBTMaPC(){
	//BTHC_05ReadByte(SERIAL_PORT_P2_CONNECTOR,&datos_recibidos_BTM);//lee datos del BT master
	UartReadByte(SERIAL_PORT_P2_CONNECTOR,&datos_recibidos_BTM);//lee datos del BT master
	LedOn(LED_3);
	//envia a PC:
	UartSendString(SERIAL_PORT_PC,UartItoa(datos_recibidos_BTM,10));
	UartSendString(SERIAL_PORT_PC,(uint8_t*)"\r");
	ActivaGraficacion();
}/*FIN BT master-->PC---*/

/****configuracion pines e inicializacion:****/
void SisInit(void){
		SystemClockInit();
	//Leds:
		LedsInit();//leds de control
	//uart:
		UartInit(&Uart_PC);
	//uart RS232 : Bluetooth Master
		BTHC_05Init(&Uart_Bluetooth_M);
		//UartInit(&Uart_Bluetooth_M);
		UartSendString(SERIAL_PORT_PC,(uint8_t*)"inicializa BT Master");
}

int main(void){

	SisInit();

    while(1){

    	/*Nada*/
    }

	return 0;
}

/*==================[end of file]============================================*/

