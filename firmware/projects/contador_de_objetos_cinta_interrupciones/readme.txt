﻿Descripción del Proyecto: Contador de objetos en cinta transportadora CON INTERRUPCIONES
Cuenta  objetos (productos) en una cinta transportadora.
mediante el TCRT 5000.
Utiliza el driver tcrt5000 y DisplayITS_E0803.
-Muestra la cantidad de objetos contados utilizando los leds como un contador binario:
 el LED_RGB_B (Azul) como el b3, el LED_1 como b2, el LED_2 como b1y el LED_3 como b0.
-Muestra la cantidad de objetos contados utilizando el display LCD.
-Se utilizan las teclas: 
   TEC1 para activar y detener el conteo.
   TEC2 para mantener el resultado (“HOLD”).
   TEC3 para resetear el conteo.