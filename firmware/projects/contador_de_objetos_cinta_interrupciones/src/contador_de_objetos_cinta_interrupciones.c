/*! @mainpage Contador_de_objetos_cinta_interrupciones
 *
 * \section genDesc General Description
 *
 *Proyecto: Contador de objetos en cinta transportadora
 *Cuenta  objetos (productos) en una cinta transportadora mediante el sensor TCRT 5000.
 *Utiliza el driver tcrt5000 y DisplayITS_E0803.
 *-Muestra la cantidad de objetos contados utilizando los leds como un contador binario:
 *el LED_RGB_B (Azul) como el b3, el LED_1 como b2, el LED_2 como b1y el LED_3 como b0.
 *-Muestra la cantidad de objetos contados utilizando el display LCD.
 *-Se utilizan las teclas:
 *      TEC1 para activar y detener el conteo.
 *      TEC2 para mantener el resultado (“HOLD”).
 *      TEC3 para resetear el conteo.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 * | 	PIN4	 	| 	GPIO_LCD_1	|
 * | 	PIN5	 	| 	GPIO_LCD_2	|
 * | 	PIN6	 	| 	GPIO_LCD_3	|
 * | 	PIN7	 	| 	GPIO_LCD_4	|
 * | 	PIN8	 	| 	GPIO_1		|
 * | 	PIN9	 	| 	GPIO_2		|
 * |	PIN9	 	| 	GPIO_3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/09/2021 | Document creation		                         |
 * | 15/09/2020	| terminado                                      |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

/*==================[inclusions]=============================================*/
#include "../../contador_de_objetos_cinta/inc/contador_de_objetos_cinta.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "tcrt5000.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"
#include "delay.h"


/*==================[macros and definitions]=================================*/
#define CANTIDAD_BITS 4
/*Banderas del estado del pin sensor,inializadas en bajo:*/
bool estado_pin=false;// false: no detecta un objeto
bool estado_anterior=false;
/*Banderas de las teclas, inializadas en bajo:*/
bool TEC1_flg= false;
bool TEC2_flg= false;
bool TEC3_flg= false;

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/
/*FUNCION para convertir un numero a binario*/
void Numero_A_Binario(uint8_t digito,uint8_t *vector_numero ){
/*Escribe el numero binario en el orden de lectura,bits mas significativo posicion 0 del vector.
* Digito es el valor que necesitamos pasar a bianrio, y lo guardamos en vector_numero*/
	uint8_t i;
	for(i=CANTIDAD_BITS;i>0;i--){
		if (digito%2==1){
			vector_numero[i-1]=1;
		}
		else{
			vector_numero[i-1]=0;
		}
	digito=digito/2;
	}
}/*FIN funcion Numero_A_Binario*/

/* FUNCIONES para interrupciones*/
void Tecla1(){
	TEC1_flg =!TEC1_flg;
}

void Tecla2(){
		TEC2_flg =!TEC2_flg;
	}

void Tecla3(){
		TEC3_flg =!TEC3_flg;
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){

/***************configuracion pines e inicializacion:*************/
	SystemClockInit();

//Sensor:
	gpio_t pin_sensor=GPIO_T_COL0; // pin de entrada al sensor
	Tcrt5000Init(pin_sensor);// inicializo el pin como entrada

//Leds:
	gpio_t pin_leds[4]={GPIO_LED_3,GPIO_LED_2,GPIO_LED_1,GPIO_LED_RGB_B};
	LedsInit();//inicializa los Leds

// Display:
	gpio_t pin_Display[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	ITSE0803Init(pin_Display);

//Teclas:
	uint8_t tecla;// variable para guardar la tecla leida
	SwitchesInit();

/*FIN configuracion e inicializacion*/

	uint8_t vector[CANTIDAD_BITS]={0};// vector con el numero binario
	uint8_t contador= 0;//variable para contar los objetos
	uint8_t contador_HOLD= 0;//variable para guardar el valor de contador
	uint8_t contador_mostrar= 0;// variable que guarda el valor a mostrar en los leds y display

/*TECLAS INTERRUPCIONES---------------------------------------------------------------------------- */
   SwitchActivInt(SWITCH_1 ,&Tecla1);
   SwitchActivInt(SWITCH_2 ,&Tecla2);
   SwitchActivInt(SWITCH_3 ,&Tecla3);

    while(1){

    	tecla = SwitchesRead();// lee la tecla y se guarda.
    	estado_pin=Tcrt5000State();//lee el estado del pin

    	if (TEC1_flg){
    		// Lee el estado actual y anterior del pin, entra al if cuando hay un cambio de 0(anterior)-->1(actual)
			if((estado_pin==true)&(estado_anterior==false)){
					contador ++;//cuenta un objeto
			}
    	}//TEC1 EN ALTO

    	if (TEC2_flg == false){
    		contador_mostrar=contador;
    	}// TEC2 esta en bajo
    	else {
    		contador_mostrar=contador_HOLD;
    	}//TEC2 esta en alto

    	if (TEC3_flg){
    		contador=0;
    	}

    	Numero_A_Binario(contador_mostrar,vector);// vector con el valor binario de contador_mostrar

/*PRENDE LEDS SEGUN NUMERO BINARIO---------------------------------------------------*/
	/*Se encienden los leds(de las posiciones correspondientes)en donde el valor del vector sea 1
	GPIO_LED_3 posicion 0...GPIO_LED_RGB_B posicion 3 del vector pin_leds.
	Utilizamos On y off de gpio, no de led.h*/
		uint8_t j;
		for(j=0;j<CANTIDAD_BITS;j++){
			if(vector[j]==1) {
				GPIOOn(pin_leds[j]);
			}
			else {
				GPIOOff(pin_leds[j]);
			}
		}
/* MUESTRA EN DISPLAY------------------------------------------------------------- */
		ITSE0803DisplayValue(contador_mostrar);

    	estado_anterior=estado_pin;
    	DelayMs(500);
    }

	return 0;
}

/*==================[end of file]============================================*/

