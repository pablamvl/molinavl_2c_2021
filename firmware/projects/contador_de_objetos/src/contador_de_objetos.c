/*! @mainpage Template
 *
 * Proyecto de prueba para el driver tcrt5000.
 * Prende LED 2 cuando encuentra un objeto.
 * Utiliza el driver tcrt5000.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/09/2021 | Document creation		                         |
 * | 09/09/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_de_objetos.h"       /* <= own header */
#include "systemclock.h"
#include "tcrt5000.h"
#include "led.h"
#include "delay.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/
bool estado_pin=false;
bool estado_anterior=false;
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	gpio_t pin=GPIO_T_COL0; // pin de entrada al sensor
	Tcrt5000Init(pin);// inicializo el pin como entrada

	SystemClockInit();
	LedsInit();
	uint16_t contador= 0;//variable para contar los objetos

    while(1){

    	estado_pin=Tcrt5000State();//lee el estado del pin

    	if((estado_pin==true)&(estado_anterior==false)){
				contador ++;
				LedOn(LED_2);// se prende el led 2,mientras cuente al objeto
    	}

    	else {
				LedOff(LED_2);
		}
    	estado_anterior=estado_pin;
    	DelayMs(500);
    }

	return 0;
}

/*==================[end of file]============================================*/

