/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * Proyecto de prueba para el driver tcrt5000.
 * Prende LED 2 cuando encuentra un objeto.
 * Utiliza el driver tcrt5000.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/09/2021 | Document creation		                         |
 * | 09/09/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

#ifndef _CONTADOR_DE_OBJETOS_H
#define _CONTADOR_DE_OBJETOS_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONTADOR_DE_OBJETOS_H */

