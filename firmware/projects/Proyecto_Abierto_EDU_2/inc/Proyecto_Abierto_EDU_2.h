/*! @mainpage Proyecto_Abierto_EDU_2
 *
 * \section genDesc Descripcion General
 * Proyecto abierto, formado por dos proyectos:
 *
 * PARTE 1: Proyecto_Abierto_EDU_1.
 * PARTE 2: Proyecto_Abierto_EDU_2.
 *
 * Descripcion del proyecto abierto en general:
 * A partir de la detección de un cambio en la aceleración del paciente por medio de un
 * sensor de aceleración triaxil(acelerómetro), de determina el inicio y final de la pasada
 * de un paciente en un laboratorio de marcha.
 *
 * Señal analógica a utilizar: señal del acelerómetro MMA7260Q, colocada en el paciente.
 * La salida de datos y el control de la aplicación, se realiza por una PC,
 * con vía comunicación con el puerto serie (Bluetooth).
 * La aplicación realizar lo siguiente:
 * - Tecla “a” activa el acelerómetro, una vez el paciente éste parado para comenzar el análisis de la marcha.
 * - Análisis de señal analógica (aceleración).
 * - Se determinará el inicio de la marcha por medio de la señal analógica.
 * - Fin de la marcha: se desactiva el acelerómetro cuando llegue a los 7m de distancia recorrida.
 *
 *
 * Función especifica del Proyecto_Abierto_EDU_2:
 * - Se determina el inicio de la marcha por medio de la señal recibida de la EDU-CIAA 1 al BT Slave.
 * - Fin de la marcha: se desactiva el acelerómetro cuando llegue a los 7m de distancia recorrida
 *
 *
 * \section hardConn Conexionado Hardware
 *
 * |  HC-05 M   	| EDU-CIAA N° 1	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 *
 * |  HC-05 S   	| EDU-CIAA N° 2	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 *
 * |  MMA7260Q   	| EDU-CIAA N° 2	|
 * |:--------------:|:--------------|
 * | 	PINX	 	| 	  CH1	    |
 * | 	PINY	 	| 	  CH2       |
 * | 	PINZ	 	| 	  CH3       |
 * | 	GS1	 	    | 	T_COL0	  	|
 * | 	GS2	       	| 	T_COL1	  	|
 * |	VCC	     	| +3.3V (VDDA)	|
 * |	GND	 	    |    GNDA       |
 * |	SM	 	    |    +3.3V      |
 *
 *
 *
 * @section changelog Registro de cambios Proyecto_Abierto_EDU_1
 *
 * |   Date	     | Description                                    |
 * |:-----------:|:-----------------------------------------------|
 * | 21/10/2021  | Inializacion  drivers                          |
 * | 27/10/2021  | configuracion de interrupcion al recibir dato  |
 * | 28/10/2021  | se agrega funcion distancia recorrida          |
 * | 03/11/2021  | compila                                        |
 * | 04/11/2021  | revisar comunicacion BT master-slave           |
 * | 04/11/2021  | compila,no hay trasmision de datos por BT      |
 * | 10/11/2021  | compila,se arregla trasmision de datos por BT  |
 * | 10/11/2021  |  se prueba comunicacion con BT celular         |
 * | 12/11/2021  |  comunicacion entre bluetooths M-S funciona    |
 * | 12/11/2021  |  se observa la grafica de acelerarcion         |
 * | 12/11/2021  |  revisar calculo distancia                     |
 * | 12/11/2021  |  calculo distancia  funcionando                |
 * | 14/11/2021  |  proyecto  funcionando                         |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

#ifndef _PROYECTO_ABIERTO_EDU_2_H
#define _PROYECTO_ABIERTO_EDU_2_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PROYECTO_ABIERTO_EDU_2_H*/

