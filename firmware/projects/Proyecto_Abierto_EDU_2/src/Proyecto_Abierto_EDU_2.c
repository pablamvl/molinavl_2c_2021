/*! @mainpage Proyecto_Abierto_EDU_2
 *
 * \section genDesc Descripcion General
 * Proyecto abierto, formado por dos proyectos:
 *
 * PARTE 1: Proyecto_Abierto_EDU_1.
 * PARTE 2: Proyecto_Abierto_EDU_2.
 *
 * Descripcion del proyecto abierto en general:
 * A partir de la detección de un cambio en la aceleración del paciente por medio de un
 * sensor de aceleración triaxil(acelerómetro), de determina el inicio y final de la pasada
 * de un paciente en un laboratorio de marcha.
 *
 * Señal analógica a utilizar: señal del acelerómetro MMA7260Q, colocada en el paciente.
 * La salida de datos y el control de la aplicación, se realiza por una PC,
 * con vía comunicación con el puerto serie (Bluetooth).
 * La aplicación realizar lo siguiente:
 * - Tecla “a” activa el acelerómetro, una vez el paciente éste parado para comenzar el análisis de la marcha.
 * - Análisis de señal analógica (aceleración).
 *	○	Se determinará el inicio de la marcha por medio de la señal analógica.
 *	○	Fin de la marcha: se desactiva el acelerómetro cuando llegue a los 7m de distancia recorrida.
 *
 *
 * Función especifica del Proyecto_Abierto_EDU_2:
 * - Se determina el inicio de la marcha por medio de la señal recibida de la EDU-CIAA 1 al BT Slave.
 * - Fin de la marcha: se desactiva el acelerómetro cuando llegue a los 7m de distancia recorrida
 *
 *
 * \section hardConn Conexionado Hardware
 *
 * |  HC-05 M   	| EDU-CIAA N° 1	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 *
 * |  HC-05 S   	| EDU-CIAA N° 2	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 *
 * |  MMA7260Q   	| EDU-CIAA N° 2	|
 * |:--------------:|:--------------|
 * | 	PINX	 	| 	  CH1	    |
 * | 	PINY	 	| 	  CH2       |
 * | 	PINZ	 	| 	  CH3       |
 * | 	GS1	 	    | 	T_COL0	  	|
 * | 	GS2	       	| 	T_COL1	  	|
 * |	VCC	     	| +3.3V (VDDA)	|
 * |	GND	 	    |    GNDA       |
 * |	SM	 	    |    +3.3V      |
 *
 *
 *
 * @section changelog Registro de cambios Proyecto_Abierto_EDU_1
 *
 * |   Date	     | Description                                    |
 * |:-----------:|:-----------------------------------------------|
 * | 21/10/2021	 | Document creation		                      |
 * | 21/10/2021  | Inializacion  drivers                          |
 * | 27/10/2021  | configuracion de interrupcion al recibir dato  |
 * | 28/10/2021  | se agrega funcion distancia recorrida          |
 * | 03/11/2021  | compila                                        |
 * | 04/11/2021  | revisar comunicacion BT master-slave           |
 * | 04/11/2021  | compila,no hay trasmision de datos por BT      |
 * | 10/11/2021  | compila,se arregla trasmision de datos por BT  |
 * | 10/11/2021  |  se prueba comunicacion con BT celular         |
 * | 12/11/2021  |  comunicacion entre bluetooths M-S funciona    |
 * | 12/11/2021  |  se observa la grafica de acelerarcion         |
 * | 12/11/2021  |  revisar calculo distancia                     |
 * | 12/11/2021  |  calculo distancia  funcionando                |
 * | 14/11/2021  |  proyecto  funcionando                         |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Abierto_EDU_2.h"       /* <= own header */
#include "systemclock.h"
#include "bool.h"
#include "led.h"
#include "timer.h"
#include "BT_HC_05.h"
#include "MMA7260Q.h"
/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define BAUD_RATE 115200 //Velocidad de transmisión
#define FRECUENCIA_MUESTREO 4 //Frecuencia de muestreo de la señal
#define TIEMPO_MUESTREO 250
#define CAMBIO_ACELERACION_INICIO 0.4// Valor que se utiliza como umbral
#define FIN_PASADA 7 // Fin distancia[m] recorrida de la marcha
#define ESCALA 100//valor que multiplico al valor de aceleracion
#define GRAVEDAD 9.8
#define TIEMPO_SEG 0.25 //valor de tiempo en segundos

/*==================[internal functions declaration]=========================*/
void UartBTS(void);//funcion lee el dato recibido por BT Slave
void Tiempo (void);//funcion de interrupcion al timer
float DistanciaRecorrida ();//calcula la distancia recorrida a apartir de la aceleracion
/*==================[internal data definition]===============================*/
uint8_t dato_recibido;// dato recibido desde el BT Master
bool tiempo_FLAG=FALSE;//bandera de tiempo
bool encendido_MMA7260Q= FALSE; //bandera de encendido-apagado del sensor MMA7260Q
float Umbral;//valor para comparar cambio en la posicion x, movimiento hacia delante
float aceleracion_x;// datos de la aceleracion en x, movimiento hacia delante
float distancia_recorrida;// variable que guarda la disntancia recorrida
float distancia_recorrida_mostar;//variable para control, ver el valor de la distancia
float distancia_actual;// variable que guarda la disntancia calculada en cada ciclo
float aceleracion;// valor de aceleracion en unidades m/s2
float tiempo_seg=TIEMPO_MUESTREO/1000;// tiempo de muestreo en segundos
/*==================[external data definition]===============================*/
gpio_t gSelect1= GPIO_T_COL0;//pin 1 de selección del driver MMA7260Q
gpio_t gSelect2= GPIO_T_COL1;//pin 2 de selección del driver MMA7260Q

serial_config UART_Bluetooth_S = {SERIAL_PORT_P2_CONNECTOR,BAUD_RATE,&UartBTS};
timer_config temporizador = {TIMER_A,TIEMPO_MUESTREO,&Tiempo};
/*==================[external functions definition]==========================*/
/****Funcion interrupcion uart****/
void UartBTS (void){
		//BTHC_05ReadByte(SERIAL_PORT_P2_CONNECTOR,&dato_recibido);//lee dato recibido
		UartReadByte(SERIAL_PORT_P2_CONNECTOR, &dato_recibido);//lee dato recibido
		LedOn(LED_RGB_G);//entra ala interrupcion
		switch (dato_recibido){
				case 'i'://comienza toma de datos del acelerometro
					LedOn(LED_1);// detecta el dato recibido INICIO
					encendido_MMA7260Q = TRUE;
				break;
	/*otros casos para agregar
				case 'f':// fin de medicion, sin haber llegado a los 7 metros
					LedOn(LED_2);// detecta el dato recibido FIN
					encendido_MMA7260Q = FALSE;
				break;
	*/
	}
}

/****calcula distancia recorrida con la señal del acelerometro****/
float DistanciaRecorrida (){
// X=(a*t2)/2 MRUV	, Vo=0; t en segundos
	aceleracion=(aceleracion_x*GRAVEDAD)/ESCALA;
	distancia_actual=(aceleracion*(TIEMPO_SEG*TIEMPO_SEG))/2;
	distancia_recorrida=distancia_recorrida+distancia_actual;

	return distancia_recorrida;
}

/****Funcion interrupcion timer****/
void Tiempo(){
	tiempo_FLAG=TRUE;
}

/****configuracion pines e inicializacion:****/
void SisInit(void){
		SystemClockInit();
		LedsInit();//leds de control
	//Bluetooth Slave:
		BTHC_05Init(&UART_Bluetooth_S);
		BTHC_05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t*)"inicializa BT Slave ");
	//Sensor:
		MMA7260QInit(gSelect1, gSelect2);
	//Timer:
		TimerInit(&temporizador);
		TimerStart(TIMER_A);
}


int main(void){
	SisInit();
	distancia_recorrida=0;
	Umbral=CAMBIO_ACELERACION_INICIO*ESCALA;//500

    while(1){
    	while(!encendido_MMA7260Q){
    	}

    	if (tiempo_FLAG){
			aceleracion_x=ReadXValue()*ESCALA;////lee datos del acelerometro

			if ((aceleracion_x)>Umbral){
				ReadXValue();//lee datos

				if (distancia_recorrida>FIN_PASADA){
					encendido_MMA7260Q = FALSE;
					LedOn(LED_RGB_G);//verde: llegó a la distancia
					distancia_recorrida=0;
				}
				else{
					// se envían los datos por BT S a BT M

					BTHC_05SendString(SERIAL_PORT_P2_CONNECTOR, UartItoa((uint32_t)aceleracion_x,10));//multiplicamos por 100
					BTHC_05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t*)",");

					/*prueba por bluetooth del celu
				    UartSendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t*)"aceleracion:");//prueba de que envía
					UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa((uint32_t)aceleracion_x,10));//multiplicamos por 100
					UartSendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t*)"\r\n");
					*/

					distancia_recorrida=DistanciaRecorrida();
					//prueba muestra distancia recorrida por bluetooth del celu
					/*
					distancia_recorrida_mostar=distancia_recorrida*ESCALA;
					BTHC_05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t*)"distancia recorrida:");
					BTHC_05SendString(SERIAL_PORT_P2_CONNECTOR, UartItoa((uint32_t)distancia_recorrida_mostar,10));
					UartSendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t*)"\r\n");
                    */
				}
				//LedOff(LED_RGB_G);
			}

			tiempo_FLAG=FALSE;
    	}
    }

	return 0;
}
/*==================[end of file]============================================*/

