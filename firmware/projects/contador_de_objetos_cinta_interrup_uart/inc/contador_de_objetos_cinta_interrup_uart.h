/*! @mainpage Contador_de_objetos_cinta_interrup_uart
 *
 * \section genDesc General Description
 *
 *Proyecto: Contador de objetos en cinta transportadora
 *Cuenta  objetos (productos) en una cinta transportadora mediante el sensor TCRT 5000.
 *La aplicacion utiliza interrupciones, timer y comunicación con puerto serie.
 *Utiliza el driver de dispositivos tcrt5000 y DisplayITS_E0803.
 *-Muestra la cantidad de objetos contados utilizando los leds como un contador binario:
 *el LED_RGB_B (Azul) como el b3, el LED_1 como b2, el LED_2 como b1y el LED_3 como b0.
 *-Muestra la cantidad de objetos contados utilizando el display LCD.
 *-Se utilizan las teclas:
 *      TEC1 para activar y detener el conteo.
 *      TEC2 para mantener el resultado (“HOLD”).
 *      TEC3 para resetear el conteo.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   tcrt5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 *
 * |DisplayITS_E0803|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN4	 	| 	GPIO_LCD_1	|
 * | 	PIN5	 	| 	GPIO_LCD_2	|
 * | 	PIN6	 	| 	GPIO_LCD_3	|
 * | 	PIN7	 	| 	GPIO_LCD_4	|
 * | 	PIN8	 	| 	GPIO_1		|
 * | 	PIN9	 	| 	GPIO_2		|
 * |	PIN9	 	| 	GPIO_3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                            |
 * |:----------:|:-------------------------------------------------------|
 * | 15/09/2020	 | Document creation		                             |
 * | 22/09/2020	 | Configurated and tested using interruption and timer  |
 * | 23/09/2020	 | Configuration using serial port added	             |
 * | 23/09/2020	 | Document completed		                             |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

#ifndef _CONTADOR_DE_OBJETOS_CINTA_INTERRUP_UART_H
#define _CONTADOR_DE_OBJETOS_CINTA_INTERRUP_UART_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONTADOR_DE_OBJETOS_CINTA_INTERRUP_UART_H*/

