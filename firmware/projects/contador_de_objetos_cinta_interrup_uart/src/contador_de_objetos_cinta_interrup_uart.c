/*! @mainpage Contador_de_objetos_cinta_interrup_uart
 *
 * \section genDesc General Description
 *
 *Proyecto: Contador de objetos en cinta transportadora
 *Cuenta  objetos (productos) en una cinta transportadora mediante el sensor TCRT 5000.
 *Utiliza el driver de dispositivos tcrt5000 y DisplayITS_E0803.
 *La aplicacion utiliza interrupciones, timer y comunicación con puerto serie.
 *-Muestra la cantidad de objetos contados utilizando los leds como un contador binario:
 *el LED_RGB_B (Azul) como el b3, el LED_1 como b2, el LED_2 como b1y el LED_3 como b0.
 *-Muestra la cantidad de objetos contados utilizando el display LCD.
 *-Se utilizan las teclas:
 *      TEC1 para activar y detener el conteo.
 *      TEC2 para mantener el resultado (“HOLD”).
 *      TEC3 para resetear el conteo.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   tcrt5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	+5V 		|
 * | 	PIN2	 	| 	TCOL0		|
 * | 	PIN3	 	| 	GND			|
 *
 * |DisplayITS_E0803|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN4	 	| 	GPIO_LCD_1	|
 * | 	PIN5	 	| 	GPIO_LCD_2	|
 * | 	PIN6	 	| 	GPIO_LCD_3	|
 * | 	PIN7	 	| 	GPIO_LCD_4	|
 * | 	PIN8	 	| 	GPIO_1		|
 * | 	PIN9	 	| 	GPIO_2		|
 * |	PIN9	 	| 	GPIO_3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                            |
 * |:----------:|:-------------------------------------------------------|
 * | 15/09/2020	 | Document creation		                             |
 * | 22/09/2020	 | Configurated and tested using interruption and timer  |
 * | 23/09/2020	 | Configuration using serial port added	             |
 * | 23/09/2020	 | Document completed		                             |
 *
 * @author Molina van Leeuwen Ana Pabla
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_de_objetos_cinta_interrup_uart.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "tcrt5000.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"
#include "delay.h"
#include "timer.h"


/*==================[macros and definitions]=================================*/
#define CANTIDAD_BITS 4

gpio_t pin_sensor=GPIO_T_COL0; // pin de entrada al sensor
gpio_t pin_leds[4]={GPIO_LED_3,GPIO_LED_2,GPIO_LED_1,GPIO_LED_RGB_B};
gpio_t pin_Display[7]= {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};

/*==================[internal data definition]===============================*/
/*Banderas del estado del pin sensor,inializadas en bajo:*/
bool estado_pin=false;// false: no detecta un objeto
bool estado_anterior=false;
/*Banderas de las teclas, inializadas en bajo:*/
bool TEC1_flg= false;
bool TEC2_flg= false;
bool TEC3_flg= false;

/*==================[internal functions declaration]=========================*/
void CuentaObejtos();
void NumeroABinario(uint8_t digito,uint8_t *vector_numero );
void DatosAPc();

/*==================[external data definition]===============================*/
uint8_t tecla;
uint8_t lineas;
uint8_t vector[CANTIDAD_BITS]={0};// vector con el numero binario
uint8_t contador= 0;//variable para contar los objetos
uint8_t contador_HOLD= 0;//variable para guardar el valor de contador
uint8_t contador_mostrar= 0;// variable que guarda el valor a mostrar en los leds y display

timer_config my_timer = {TIMER_A,1000,&CuentaObejtos};
serial_config UART_USB = {SERIAL_PORT_PC,115200,&DatosAPc};

/*==================[external functions definition]==========================*/
/*FUNCION para convertir un numero a binario*/
void NumeroABinario(uint8_t digito,uint8_t *vector_numero ){
/*Escribe el numero binario en el orden de lectura,bits mas significativo posicion 0 del vector.
* Digito es el valor que necesitamos pasar a bianrio, y lo guardamos en vector_numero*/
	uint8_t i;
	for(i=CANTIDAD_BITS;i>0;i--){
		if (digito%2==1){
			vector_numero[i-1]=1;
		}
		else{
			vector_numero[i-1]=0;
		}
	digito=digito/2;
	}
}/*FIN funcion Numero_A_Binario*/

/* FUNCIONES para interrupciones*/
void Tecla1(){
	TEC1_flg =!TEC1_flg;
}

void Tecla2(){
		TEC2_flg =!TEC2_flg;
}

void Tecla3(){
		TEC3_flg =!TEC3_flg;
}

/*FUNCION que se ejecuta ene el timer*/
void CuentaObejtos(){

//	tecla = SwitchesRead();// lee la tecla y se guarda.
	estado_pin=Tcrt5000State();//lee el estado del pin

	if (TEC1_flg){
		// Lee el estado actual y anterior del pin, entra al if cuando hay un cambio de 0(anterior)-->1(actual)
		if((estado_pin==true)&(estado_anterior==false)){
				contador ++;//cuenta un objeto
		}
	}//TEC1 EN ALTO

	if (TEC2_flg == false){
		contador_mostrar=contador;
	}// TEC2 esta en bajo
	else {
		contador_mostrar=contador_HOLD;
	}//TEC2 esta en alto

	if (TEC3_flg){
		contador_mostrar=0;
	}

	NumeroABinario(contador_mostrar,vector);// vector con el valor binario de contador_mostrar

/*PRENDE LEDS SEGUN NUMERO BINARIO---------------------------------------------------*/
/*Se encienden los leds(de las posiciones correspondientes)en donde el valor del vector sea 1
GPIO_LED_3 posicion 0...GPIO_LED_RGB_B posicion 3 del vector pin_leds.
Utilizamos On y off de gpio, no de led.h*/
	uint8_t j;
	for(j=0;j<CANTIDAD_BITS;j++){
		if(vector[j]==1) {
			GPIOOn(pin_leds[j]);
		}
		else {
			GPIOOff(pin_leds[j]);
		}
	}
/* MUESTRA EN DISPLAY------------------------------------------------------------- */
	ITSE0803DisplayValue(contador_mostrar);
	estado_anterior=estado_pin;

}
/*FUNCION:*/
void DatosAPc(){

	UartReadByte(SERIAL_PORT_PC,&lineas);

	switch(lineas){

				case 'o' :
					Tecla1();
				break;

				case 'h' :
					Tecla2();
				break;

				case '0' :
					Tecla3();
				break;
	}
}

void SisInit(void){
	/***************configuracion pines e inicializacion:*************/
		SystemClockInit();
	//Sensor:
		Tcrt5000Init(pin_sensor);// inicializo el pin como entrada
	//Leds:
		LedsInit();//inicializa los Leds
	// Display:
		ITSE0803Init(pin_Display);
	//Teclas:
		SwitchesInit();
	//Timer
		TimerInit(&my_timer);
		TimerStart(TIMER_A);
	//uart
		UartInit(&UART_USB);

}

int main(void){

/***************configuracion pines e inicializacion:*************/
	SisInit();
	//Teclas:
    SwitchActivInt(SWITCH_1 ,&Tecla1);
    SwitchActivInt(SWITCH_2 ,&Tecla2);
    SwitchActivInt(SWITCH_3 ,&Tecla3);

    //uart:

    lineas= UartItoa(contador_mostrar, 10);

    UartSendString (SERIAL_PORT_PC, lineas );//a uartItoa manda el valor del contador y la base en la que esta escrito el valor
    UartSendString (SERIAL_PORT_PC,"lineas \n\r");
    while(1){

    	/*Nada*/
    }

	return 0;
}

/*==================[end of file]============================================*/

