
/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 18--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*========================[macros and definitions]=================================*/
#define EJERCICIO_18

#define CANTIDAD 15
uint8_t Valor=0;
uint8_t promedio=0;
uint16_t acumulador=0;
uint8_t i;
uint8_t valores[CANTIDAD]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
/*=======================[internal functions declaration]===========================*/
int main(void)
{
// EJERCICIO 18 ****************************************************************************/
/*
Al ejercicio anterior agregue un número más (16 en total), modifique su programa de manera que
agregue el número extra a la suma e intente no utilizar el operando división “/”
N°16 =>233. Si utiliza las directivas de preprocesador ( #ifdef, #ifndef,#endif, etc) no necesita
generar un nuevo programa.
*/
	for(i=0;i<CANTIDAD;i++){

		acumulador=acumulador+valores[i];

	}
	printf("acumulado_1:%d\r\n", acumulador);

#ifndef EJERCICIO_18
#define EJERCICIO_17
	printf("----------EJERCICIO 17---------- \r\n");

	promedio=(acumulador/CANTIDAD);
	printf("Ejer.17:promedio es:%d \r\n\n", promedio);
#endif

#ifdef EJERCICIO_18
#define EJERCICIO_18
	printf("----------EJERCICIO 18---------- \r\n");
	Valor=233;
//	acumulador=acumulador+Valor;
	printf("acumulado_2:%d\r\n", acumulador);
	promedio=acumulador>>4;// me muevo al 4 bits que significa dividir por 16

	printf("Ejer.18:El promedio es:%d", promedio);

#endif

	return 0;

}

/*==================[end of file]============================================*/

