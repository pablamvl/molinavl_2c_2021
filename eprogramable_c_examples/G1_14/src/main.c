/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 14--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*========================[macros and definitions]=================================*/


/*=======================[internal functions declaration]===========================*/
//estructura “alumno”, con “nombre” de 12 caracteres, “apellido” de 20,edad:
	struct alumno { //tipo de estructura
// typedef struct alumno {
		char nombre[12];
		char apellido[20];
		int edad;
	};//puedo definir aca una variable de este tipo

int main(void)
{
// EJERCICIO 14 ****************************************************************************/
/*
Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
a.Defina una variable con esa estructura y cargue los campos con sus propios datos.
b.Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).
 */
	printf("----------EJERCICIO 14---------- \r\n");
	struct alumno alumno_1;
// alumno alumno_1:
	strcpy(alumno_1.nombre,"Pabla");
	strcpy( alumno_1.apellido,"Molina");
    alumno_1.edad=30;

	printf("alumno:\r\n");
	printf("nombre:%s\r\n", alumno_1.nombre);
	printf("apellido: %s \r\n", alumno_1.apellido);
	printf("edad: %d \r\n\n", alumno_1.edad);
//
	struct alumno *alumno_puntero;
	struct alumno alumno_2;
	alumno_puntero= &alumno_2;

// alumno alumno_2:
	strcpy(alumno_puntero->nombre,"Azul");
	strcpy( alumno_puntero->apellido,"Acosta");
	alumno_puntero->edad=24;

	printf("alumno:\r\n");
	printf("nombre:%s\r\n", alumno_puntero->nombre);
	printf("apellido: %s \r\n", alumno_puntero->apellido);
	printf("edad: %d \r\n", alumno_puntero->edad);

	return 0;

}


/*==================[end of file]============================================*/

