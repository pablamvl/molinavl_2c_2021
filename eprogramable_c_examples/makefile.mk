########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

####Guia 1------------------: 
#PROYECTO_ACTIVO = G1_1
#NOMBRE_EJECUTABLE = G1_1.exe

#PROYECTO_ACTIVO = G1_2
#NOMBRE_EJECUTABLE = G1_2.exe

#PROYECTO_ACTIVO = G1_7
#NOMBRE_EJECUTABLE = G1_7.exe

#PROYECTO_ACTIVO = G1_9
#NOMBRE_EJECUTABLE = G1_9.exe

#PROYECTO_ACTIVO = G1_12
#NOMBRE_EJECUTABLE = G1_12.exe

#PROYECTO_ACTIVO = G1_14
#NOMBRE_EJECUTABLE = G1_14.exe

#PROYECTO_ACTIVO = G1_16
#NOMBRE_EJECUTABLE = G1_16.exe

#PROYECTO_ACTIVO = G1_17
#NOMBRE_EJECUTABLE = G1_17.exe

#PROYECTO_ACTIVO = G1_18
#NOMBRE_EJECUTABLE = G1_18.exe

####Guia 1_Integradores: 
#PROYECTO_ACTIVO = G1_A
#NOMBRE_EJECUTABLE = G1_A.exe

####Guia 1_Integradores: 
#PROYECTO_ACTIVO = G1_C
#NOMBRE_EJECUTABLE = G1_C.exe


####Guia 1_Integradores: 
PROYECTO_ACTIVO = G1_D
NOMBRE_EJECUTABLE = G1_D.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe
