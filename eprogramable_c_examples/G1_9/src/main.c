/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 9--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*========================[macros and definitions]=================================*/
	#define FALSE 0
	//#define TRUE 1
//Mascara:
	const uint32_t MSK_3= 1<<4;
//Constante:
	//const int32_t CONST;// constante de 32 bits
	//const int32_t CONST=50;
	const int32_t CONST=62;
	int32_t A =0;
/*=======================[internal functions declaration]===========================*/

int main(void)
{
// EJERCICIO 9****************************************************************************/
/* Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0.
 * Si es 0, cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa.
 *
 */
	printf("----------EJERCICIO 9---------- \r\n");

	if((CONST & MSK_3) == FALSE){
		printf("La constante %d tiene en el bit 4 un 0 ,entonces la variable A es: %d \r\n",CONST,A);
	}
	else {
		A =0Xaa;
		printf("La constante %d tiene en el bit 4 un 1 ,entonces la variable A es: %d  \r\n",CONST,A);
	}

	return 0;
}


/*==================[end of file]============================================*/

