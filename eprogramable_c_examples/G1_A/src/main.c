/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio A--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
//#include <string.h>

/*========================[macros and definitions]=================================*/

typedef enum{
	ON=1,
	OFF=2,
	TOGGLE=3,
}modo_t;

typedef enum{//empieza a enumerar desde 0, por eso le asigno 1,2 y3
	LED_1=1,
	LED_2=2,
	LED_3=3,
}led_t;

/*=======================[internal functions declaration]===========================*/

//ESTRUCTURA
typedef struct{
	uint8_t n_led;  //indica el número de led a controlar
	uint8_t n_ciclos; //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;// indica el tiempo de cada ciclo
	uint8_t mode;// ON, OFF, TOGGLE
} my_leds;

//FUNCION
void Control_Led (my_leds*leds){
//	printf(" entra  %d \r\n",leds->n_led);
	uint8_t i;
	uint8_t j;
	switch(leds->mode){// primer arbol
	case ON://----------------------------------------------ON
	printf(" entra ON  \r\n");
		switch(leds->n_led){// segundo arbol
			case LED_1:
				printf(" Enciende LED %d \r\n",leds->n_led);
				break;

			case LED_2:
				printf(" Enciende LED %d \r\n",leds->n_led);
				break;

			case LED_3:
				printf("Enciende LED %d  \r\n",leds->n_led);
				break;
		}
		break;

	case OFF://----------------------------------------------OFF
	printf(" entra OFF  \r\n");
		switch(leds->n_led){
		case LED_1:
			printf("Apaga LED %d \r\n",leds->n_led);
			break;

		case LED_2:
			printf("Apaga LED %d \r\n",leds->n_led);
			break;

		case LED_3:
			printf(" Apaga LED %d \r\n",leds->n_led);
			break;
		}
		break;

	case TOGGLE://-----------------------------------------TOGGLE
	printf(" entra TOGGLE \r\n");
		for(i=0;i<leds->n_ciclos;i++){
			switch(leds->n_led){
			case LED_1:
				printf("Toogle LED %d \r\n",leds->n_led);
				break;

			case LED_2:
				printf("Toogle LED %d \r\n",leds->n_led);
				break;

			case LED_3:
				printf("Toogle LED %d \r\n",leds->n_led);
				break;
			}
			// se incrementa hasta retardos, tiempo de parpadeo
			for(j=0;j<leds->periodo;j++){
				printf("Retardo \r\n");
			}
		}
		break;
	}
}

int main(void)
{
	my_leds led;
//INICIALIZO MI LED:
	led.n_led=2;
	led.n_ciclos=3;
	led.periodo=3;
	led.mode=3;

	Control_Led (&led);

	return 0;
}

// enciende led 2 ,3 veces
/*==================[end of file]============================================*/

