/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio C--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/
/*Escribir una función que reciba un dato de 32 bits,  la cantidad de dígitos de
 salida y un puntero a un arreglo donde se almacene los n dígitos. La función deberá
 convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida en
 el arreglo pasado como puntero.
 */
/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*========================[macros and definitions]=================================*/
//	uint32_t dato= 12;
//	uint8_t cantidad_digitos=2;
	uint32_t dato= 124;
	uint8_t cantidad_digitos=3;
/*=======================[internal functions declaration]===========================*/

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
//data : numero
//digits: cantidad de digitos
//bcd_number: el numero pasado a bcd
	uint8_t resultado;
	uint8_t  i;
	for(i=digits; i>0; i--){
		resultado=data%10;
		bcd_number [(i-1)]=resultado;
		data=data/10;
		}

	return 0;
}

int main(void)
{

	printf(" dato: %d\r\n", dato);
	printf(" cantidad de digitos: %d\r\n", cantidad_digitos);

	uint8_t bcd_numero[cantidad_digitos];

	BinaryToBcd (dato,cantidad_digitos,bcd_numero);

	uint8_t  i;
	for(i=0;i<cantidad_digitos; i++){

		printf("digito %d:%d\r\n",i+1, bcd_numero[(i)]);
	}

	return 0;
}


/*==================[end of file]============================================*/

