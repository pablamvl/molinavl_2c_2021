/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1: NO OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 1--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*===================[inclusions]============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/

int main(void)
{
// EJERCICIO 1-------------------------------------------------------------------------
//Declare una constante de 32 bits, con todos los bits en 0 y el bit 6 en 1. <<.
	printf("-----EJERCICIO 1----- \r\n");
	int32_t const1 =0 | 1<<6;//constante de 32 bits
	printf("La constante con todos los bits en 0 y el bit 6 en 1 es : %d  \r\n",const1);
	return 0;
}

/*==================[end of file]============================================*/

