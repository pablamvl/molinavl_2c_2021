/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 16--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
//#include <string.h>

/*========================[macros and definitions]=================================*/
//Variable:
	uint32_t variable_x= 0x01020304;// variable sin signo de 32 bits
	//variable_x = 0x01020304;// 32 bits
//otras variablesde 8 bits:
	uint8_t variable_x1;
	uint8_t variable_x2;
	uint8_t variable_x3;
	uint8_t variable_x4;
/*//Mascaras:
	uint32_t MSK_V1=0x000000FF;
	uint32_t MSK_V2=0x000000FF;
	uint32_t MSK_V3=0x00FF0000;
	uint32_t MSK_V4=0xFF000000;
*/

/*=======================[internal functions declaration]===========================*/
	union Union_x {
			struct{
				  uint8_t var_1;
				  uint8_t var_2;
				  uint8_t var_3;
				  uint8_t var_4;
				};

				uint32_t variable_32bits;
	};

int main(void)
{
// EJERCICIO 16 ****************************************************************************/
/*
 Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 Declare cuatro variables sin signo de 8 bits.
 Utilizando máscaras, rotaciones y truncamiento,
 cargue cada uno de los bytes de la variable de 32 bits.
 Realice el mismo ejercicio, utilizando la definición de una “union”.
 *
 */
	printf("----------EJERCICIO 16---------- \r\n");

//primera parte:------------------------
	printf("Primera parte:---------- \r\n");
	variable_x4= (uint8_t)variable_x;
	variable_x3= (uint8_t)(variable_x >> 8);
	variable_x2= (uint8_t)(variable_x >> 16);
	variable_x1= (uint8_t)(variable_x >> 24);
/*//mascaras:
	variable_x4= (uint8_t)(variable_x & MSK_V1);
	variable_x3= (uint8_t)(variable_x & MSK_V2);
	variable_x2= (uint8_t)(variable_x & MSK_V3);
	variable_x1= (uint8_t)(variable_x & MSK_V4);
*/
	printf("variable de 32 bits:%d  \r\n", variable_x);
	printf("variable 4 :%d  \r\n", variable_x4);
	printf("variable 3 :%d  \r\n", variable_x3);
	printf("variable 2:%d  \r\n", variable_x2);
	printf("variable 1:%d  \r\n ", variable_x1);

//segunda parte:------------------------
	printf("Segunda parte:---------- \r\n");
	union Union_x union_Var32;

	union_Var32.variable_32bits=0x01020304;
	printf("variable 32bits:%d \r\n ", union_Var32.variable_32bits);

	printf("var_4:%d \r\n", union_Var32.var_4);
	printf("var_3:%d \r\n", union_Var32.var_3);
	printf("var_2:%d \r\n", union_Var32.var_2);
	printf("var_1:%d \r\n", union_Var32.var_1);


	return 0;
}



/*==================[end of file]============================================*/

