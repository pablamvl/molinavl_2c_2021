/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 12--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*========================[macros and definitions]=================================*/
	int16_t *puntero;//puntero a un entero con signo de 16 bits
	int16_t variable= -1;
//Mascara:1111 1111 1110 1111
	const uint16_t MSK_4= ~(1<<4);//coloca un 0 en el bit 4

/*=======================[internal functions declaration]===========================*/

int main(void)
{
// EJERCICIO 12 ****************************************************************************/
/*Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1.
 * Luego, mediante máscaras, coloque un 0 en el bit 4
 */
	printf("----------EJERCICIO 12---------- \r\n");

	puntero=&variable;
	*puntero=*puntero&MSK_4;
	printf("La mascara:%d Coloca un 0 en el bit 4 del entero apuntado: %d \r\n",MSK_4,*puntero);

	return 0;
}

/*==================[end of file]============================================*/

