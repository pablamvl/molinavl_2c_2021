/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio 17--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*========================[macros and definitions]=================================*/

#define CANTIDAD 15

/*=======================[internal functions declaration]===========================*/

int main(void)
{

// EJERCICIO 17 ****************************************************************************/
/*
Realice un programa que calcule el promedio de los 15 números listados abajo, para ello,
primero realice un diagrama de flujo similar al presentado en el ejercicio 9.
(Puede utilizar la aplicación Draw.io).
Para la implementación, utilice el menor tamaño de datos posible:
 */
	printf("----------EJERCICIO 17---------- \r\n");
	uint8_t valores[15]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
	uint8_t i;
	uint8_t promedio=0;
	uint16_t acumulador=0;

	for(i=0;i<CANTIDAD;i++){

		acumulador=acumulador+valores[i];

	}
	promedio=(acumulador/CANTIDAD);

	printf(" El promedio es:%d", promedio);

	return 0;

}


/*==================[end of file]============================================*/

