/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1:OBLIGATORIOS -----------------------
 *-------------------------------- Ejercicio D--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/
/*Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.
*/
/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
//#include <string.h>
/*========================[macros and definitions]=================================*/

# define OUT 1
# define IN 0
//uint8_t BCD=9;
uint8_t BCD=4;

/*=======================[internal functions declaration]===========================*/
typedef struct{
	uint8_t port;/*!< GPIO port number */
	uint8_t pin;/*!< GPIO pin number */
	uint8_t dir;/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

//FUNCION:
void Configuracion_puertos(uint8_t digito, gpioConf_t *puertos_pines){
	uint8_t i;
	for(i=0;i<4;i++){
		//printf("digito %d\r\n",digito);
		if (digito%2==1){

			printf("bit[%d]->Puerto:%d.%d:1\r\n",i, puertos_pines[i].port,puertos_pines[i].pin);
		}
		else{

			printf("bit[%d]->Puerto:%d.%d:0\r\n",i, puertos_pines[i].port,puertos_pines[i].pin);
		}

	digito=digito/2;
	}
}

int main(void)
{
	gpioConf_t puertos_pines [4]={
	//inicializo el vector de estructura
		{1,4,OUT},//b0 -> puerto 1.4
		{1,5,OUT},//b1 -> puerto 1.5
		{1,6,OUT},//b2-> puerto 1.6
		{2,14,OUT},//b3-> puerto 2.14
	};

	Configuracion_puertos(BCD,puertos_pines);

	return 0;
}


/*==================[end of file]============================================*/

