/*****************************************************************************
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 *---------------------------- GUIA 1: OBLIGATORIOS --------------------------
 *-------------------------------- Ejercicio 7--------------------------------
 * Autor: Pabla M.v.L
 *****************************************************************************/

/*========================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*=======================[macros and definitions]=================================*/
//Variable de 32 bits sin signo:
	//int32_t variable ;
	int32_t variable=6;
	//uint32_t variable=65535;
//Mascaras:
	const uint32_t MSK_1= 1<<3; // bit 3 --> 1
	const uint32_t MSK_2= ~((1<<13)|(1<<14)); // bits 13 y 14 --> 0

/*=======================[internal functions declaration]===========================*/

int main(void)
{
// EJERCICIO 7 ****************************************************************************/
 /*Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido,
 asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0 mediante máscaras y el operador<<.
 */
	printf("----------EJERCICIO 7---------- \r\n");
	printf("Variable= %u \r\n",variable);
	variable=variable|MSK_1;
	printf("La mascara:%u Coloca el bit 3 a 1 de la variable \r\n",MSK_1);

	variable= variable&MSK_2;
	printf("La mascara: %u Coloca los bits 13 y 14 a 0 de la variable \r\n",MSK_2);

	return 0;
}

/*==================[end of file]============================================*/

